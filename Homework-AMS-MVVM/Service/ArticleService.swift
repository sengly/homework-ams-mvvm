//
//  ArticleService.swift
//  Homework-AMS-MVVM
//
//  Created by Sengly Sun on 12/30/19.
//  Copyright © 2019 Sengly Sun. All rights reserved.
//

import Foundation
import Alamofire

class ArticleService{
    
    //MARK: - FETCH all Article
    func fetchArticle(page: Int, completionHanlder: @escaping ([Article])->Void){
        let url = "\(Config.FETCH_URL)\(page)&limit=20"
        Alamofire.request(url).responseData { (respone) in
            if let data = respone.data {
                do {
                    let article = try JSONDecoder().decode(DataProviding.self, from: data)
                    completionHanlder(article.data)
                } catch  {
                    completionHanlder([])
                    print("Error: \(error.localizedDescription)")
                }
            }else{
                print("Error: \(String(describing: respone.error?.localizedDescription))")
            }
        }
    }
    
    //MARK: - UPLOAD File
    func uploadFile(uploadData: Data, completionHandler: @escaping (String)->Void){
        Alamofire.upload(multipartFormData: { (multipartForm) in
            multipartForm.append(uploadData, withName: "FILE", fileName: "file.jpeg", mimeType: "image/jpeg")
        }, to: Config.UPLOAD_URL) { (encodingResult) in
            switch encodingResult{
                
            case .success(let request,_,_):
                request.responseData { (respone) in
                    let json = try? JSONSerialization.jsonObject(with: respone.data!, options: .allowFragments) as? [String: Any]
                    let url = json!["DATA"] as! String
                    completionHandler(url)
                }
            case .failure(_):
                print("UPLOAD FAILED!!")
            }
        }
    }
    
    //MARK: - POST Article
    func postArticle(article: Article,completionHandler: @escaping (Bool)-> Void){
        
        let headers = HTTPHeaders(dictionaryLiteral: ("Content-Type","application/json"),("Authorization","Basic QU1TQVBJQURNSU46QU1TQVBJUEBTU1dPUkQ="))
        
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyyMMddhhmmss"
        let dateFormatted = formatter.string(from: date)
        
        //Format date
        let params:[String: Any] = [
            "TITLE": article.title!,
            "DESCRIPTION": article.description!,
            "CREATED_DATE": dateFormatted,
            "IMAGE": article.image!
        ]
        
        Alamofire.request(Config.ARTICLE_URL, method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers).responseData { (respone) in
            if respone.error == nil{
                completionHandler(true)
                print("POST ARTICLE SUCCESSFULLY!")
            }else{
                print("ERROR: \(String(describing: respone.error?.localizedDescription))")
            }
        }
    }
    
    //MARK: - UPDATE Article
    func updateArticle(id: Int, article: Article,completionHandler: @escaping (Bool)->Void){
        let url = "\(Config.ARTICLE_URL)\(id)"
        let headers = HTTPHeaders(dictionaryLiteral: ("Content-Type","application/json"),("Authorization","Basic QU1TQVBJQURNSU46QU1TQVBJUEBTU1dPUkQ="))
        
        //Format date
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyyMMddhhmmss"
        let dateFormatted = formatter.string(from: date)
        
        let params:[String: Any] = [
            "TITLE": article.title!,
            "DESCRIPTION": article.description!,
            "CREATED_DATE": dateFormatted,
            "IMAGE": article.image!
        ]
        
        Alamofire.request(url, method: .put, parameters: params, encoding: JSONEncoding.default, headers: headers).responseData { (respone) in
            if respone.error == nil{
                completionHandler(true)
                print("UPDATE ARTICLE SUCCESSFULLY!")
            }else{
                print("ERROR: \(String(describing: respone.error?.localizedDescription))")
            }
        }
    }
    
    //MARK: - DELETE Article
    func deleteArticle(id: Int){
        
        let url = "\(Config.ARTICLE_URL)\(id)"
        let headers = HTTPHeaders(dictionaryLiteral: ("Content-Type","application/json"),("Authorization","Basic QU1TQVBJQURNSU46QU1TQVBJUEBTU1dPUkQ="))
        Alamofire.request(url, method: .delete, parameters: nil, encoding: JSONEncoding.default, headers: headers).responseData { (respone) in
            if respone.error == nil{
                print("DELETE ARTICLE ID = \(id) SUCCESSFULLY!")
            }else{
                print("ERROR: \(String(describing: respone.error?.localizedDescription))")
            }
        }
    }
    
    
}
