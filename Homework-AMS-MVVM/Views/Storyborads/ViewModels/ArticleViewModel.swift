//
//  ArticleViewModel.swift
//  Homework-AMS-MVVM
//
//  Created by Sengly Sun on 12/30/19.
//  Copyright © 2019 Sengly Sun. All rights reserved.
//

import Foundation
class ArticleViewModel{
    
    let articleService = ArticleService()
    
    //MARK: - FETCH Article
    func fetchArticle(page: Int,completionHandler:@escaping([ArticleModel])->()){
        articleService.fetchArticle(page: page) { (article) in
            var newArticle = [ArticleModel]()
            newArticle = article.compactMap(ArticleModel.init)
            completionHandler(newArticle)
        }
    }
    
    //MARK: - POST Article
    func postArticle(uploadData: Data, article: Article, completionHandler: @escaping (Bool)-> Void){
        articleService.uploadFile(uploadData: uploadData) { (url) in
            article.image = url
            self.articleService.postArticle(article: article) { (success) in
                completionHandler(success)
            }
        }
    }
    
    //MARK: - UPDATE Article
    func updateArticle(updateData: Data,id: Int, article: Article,completionHandler: @escaping (Bool)->Void){
        articleService.uploadFile(uploadData: updateData) { (url) in
            article.image = url
            self.articleService.updateArticle(id: id, article: article) { (success) in
                completionHandler(success)
            }
        }
    }
    
    //MARK: - DELETE Article
    func deleteArticle(id: Int){
        articleService.deleteArticle(id: id)
    }
}
