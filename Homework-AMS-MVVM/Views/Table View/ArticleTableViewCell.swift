//
//  ArticleTableViewCell.swift
//  Homework-AMS-MVVM
//
//  Created by Sengly Sun on 12/31/19.
//  Copyright © 2019 Sengly Sun. All rights reserved.
//

import UIKit

class ArticleTableViewCell: UITableViewCell {

    @IBOutlet var articleImageView: UIImageView!
    
    @IBOutlet var authorLabel: UILabel!
    @IBOutlet var createdDateLabel: UILabel!
    @IBOutlet var articleViewLabel: UILabel!
    @IBOutlet var articleTitleLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
