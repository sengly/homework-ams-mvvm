//
//  Extention.swift
//  Homework-AMS-MVVM
//
//  Created by Sengly Sun on 12/31/19.
//  Copyright © 2019 Sengly Sun. All rights reserved.
//

import Foundation
import UIKit
extension UIButton{
    func makeButtonRound(){
        self.bounds.size.width = self.bounds.size.height
        self.layer.cornerRadius = 0.5 * self.bounds.size.height
        self.clipsToBounds = true
    }
}

extension UIViewController{
    func makeNavigationBar(color: UIColor){
        let navBarAppearance = UINavigationBarAppearance()
        navBarAppearance.configureWithOpaqueBackground()
        navBarAppearance.largeTitleTextAttributes = [.foregroundColor: UIColor.white]
        navBarAppearance.titleTextAttributes = [.foregroundColor: UIColor.white]
        navBarAppearance.backgroundColor = color
        navigationController?.navigationBar.standardAppearance = navBarAppearance
        navigationController?.navigationBar.compactAppearance = navBarAppearance
        navigationController?.navigationBar.scrollEdgeAppearance = navBarAppearance
        navigationController?.navigationBar.tintColor = .white
    }
}
