//
//  Utility.swift
//  Homework-AMS-MVVM
//
//  Created by Sengly Sun on 1/1/20.
//  Copyright © 2020 Sengly Sun. All rights reserved.
//

import Foundation
class Utility {
    
    
    //MARK: - SAVE Image To Photo Ablum
    func saveImageToPhotoAlbum (image: UIImage){
        UIImageWriteToSavedPhotosAlbum(image, self, #selector(saveImage(_:_:)), nil)
    }
    
    //MARK: - HANDLE Save image
    @objc func saveImage(_ image: UIImage,_ error: Error?){
        if error == nil{
            print("Save Image Success")
        }else{
            print("Saving Error: \(String(describing: error?.localizedDescription))")
        }
    }
}
