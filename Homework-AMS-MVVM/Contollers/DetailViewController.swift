//
//  DetailViewController.swift
//  Homework-AMS-MVVM
//
//  Created by Sengly Sun on 12/31/19.
//  Copyright © 2019 Sengly Sun. All rights reserved.
//

import UIKit


class DetailViewController: UIViewController {

    @IBOutlet var descriptionTextView: UITextView!
    @IBOutlet var detailTitleLabel: UILabel!
    @IBOutlet var detailCreatedDateLabel: UILabel!
    @IBOutlet var typeLabel: UILabel!
    @IBOutlet var viewLabel: UILabel!
    @IBOutlet var detailImageView: UIImageView!
    
    var createdDate: String?
    var titles: String?
    var desc: String?
    var image: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpLongPress()
        self.makeNavigationBar(color: .systemTeal)
        setUpDetail()
        // Do any additional setup after loading the view.
    }
    
    //MARK: - SET UP Detail
    func setUpDetail(){
        let url = (image?.contains("http://"))! ? image! : "http://"+image!
        let imageURL = URL(string: url)
        self.detailTitleLabel?.text = titles
        self.detailCreatedDateLabel?.text = createdDate
        self.descriptionTextView?.text = desc
        self.detailImageView.kf.setImage(with: imageURL!)
    }
    
    //MARK: - SET UP Long Press Gesture On Image
    func setUpLongPress(){
        let longPress = UILongPressGestureRecognizer(target: self, action: #selector(longPressed(_:)))
        longPress.minimumPressDuration = 0.5
        self.detailImageView.addGestureRecognizer(longPress)
    }

    //MARK: - HANDLE Long Press Gesture
    @objc func longPressed(_ longPress: UILongPressGestureRecognizer){
        let saveImage = self.detailImageView.image
        let actionSheet = UIAlertController(title: "Save this image to Photo Album?", message: "", preferredStyle: .actionSheet)
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        let save = UIAlertAction(title: "Save", style: .default) { (_) in
            self.saveImageToPhotoAlbum(image: saveImage!)
            SweetAlert().showAlert("Save Image", subTitle: "You have been saved this image", style: AlertStyle.success)
        }
        actionSheet.addAction(cancel)
        actionSheet.addAction(save)
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    //MARK: - SAVE Image To Photo Ablum
    func saveImageToPhotoAlbum (image: UIImage){
        UIImageWriteToSavedPhotosAlbum(image, self, #selector(saveImage(_:_:_:)), nil)
    }
    
    //MARK: - HANDLE Save image
    @objc func saveImage(_ image: UIImage,_ error: Error?,_ context: UnsafeMutableRawPointer){
        if error == nil{
            print("Save Image Success")
        }else{
            print("Saving Error: \(String(describing: error?.localizedDescription))")
        }
    }

    
    
}
