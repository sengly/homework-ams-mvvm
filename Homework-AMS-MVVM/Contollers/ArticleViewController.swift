//
//  ArticleViewController.swift
//  Homework-AMS-MVVM
//
//  Created by Sengly Sun on 12/31/19.
//  Copyright © 2019 Sengly Sun. All rights reserved.
//

import UIKit
import Kingfisher

class ArticleViewController: UIViewController, UITableViewDelegate, UITableViewDataSource{
    

    @IBOutlet var tableView: UITableView!
    
    var page = 1
    var articles = [ArticleModel]()
    var articleViewModel = ArticleViewModel()
    var indentifier = "cell"
    override func viewDidLoad() {
        super.viewDidLoad()
        registerCell()
        setUpTapGesture()
        setUpPullToRefresh()
        self.makeNavigationBar(color: .systemTeal)
        articleViewModel.fetchArticle(page: page) { [weak self](articles) in
            guard let self = self else{return}
            DispatchQueue.main.async {
                self.articles = articles
                self.tableView.reloadData()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return articles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: indentifier, for: indexPath) as! ArticleTableViewCell
        let url = articles[indexPath.row].image.contains("http://") ? articles[indexPath.row].image : "http://"+articles[indexPath.row].image
        let imageURL = URL(string: url)
        cell.articleImageView.kf.setImage(with: imageURL!)
        cell.articleTitleLabel?.text = articles[indexPath.row].title
        cell.createdDateLabel?.text = articles[indexPath.row].createdDate
        return cell
    }

    
    //MARK: - REGISTER Cell
    func registerCell(){
        tableView.register(UINib(nibName: "ArticleTableViewCell", bundle: .main), forCellReuseIdentifier: indentifier)
        tableView.separatorStyle = .none
    }

    //MARK: - SET UP Tap Gesture
    func setUpTapGesture(){
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapGesture(_:)))
        tap.numberOfTapsRequired = 1
        tap.numberOfTouchesRequired = 1
        tableView.addGestureRecognizer(tap)
    }
    
    //MARK: - HANDLE Tap Gesture
    @objc func tapGesture(_ tapGesture: UIGestureRecognizer){
        let point = tapGesture.location(in: tableView)
        if let indexPath = tableView.indexPathForRow(at: point){
            let cell = self.tableView.cellForRow(at: indexPath) as! ArticleTableViewCell
            let title = cell.articleTitleLabel.text
            let description = articles[indexPath.row].description
            let createdDate = cell.createdDateLabel.text
            let image = articles[indexPath.row].image
            let storyboard = UIStoryboard(name: "DetialStoryboard", bundle: Bundle.main)
            let detailVC = storyboard.instantiateViewController(withIdentifier: "detialViewController") as! DetailViewController
            detailVC.createdDate = createdDate!
            detailVC.titles = title!
            detailVC.desc = description
            detailVC.image = image
            navigationController?.pushViewController(detailVC, animated: true)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        articleViewModel.fetchArticle(page: page) { [weak self](articles) in
            guard let self = self else{return}
            DispatchQueue.main.async {
                self.articles = articles
                self.tableView.reloadData()
            }
        }
    }
    
    //MARK: - Custom Swipe to edit
    func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let editButton = UIContextualAction(style: .normal, title: "") { (action, _, _) in
            let storyborad = UIStoryboard(name: "NewArticleStoryboard", bundle: .main)
            let newArticleViewController = storyborad.instantiateViewController(identifier: "newArticleViewController") as! NewArticleViewController
            newArticleViewController.isEdit = true
            newArticleViewController.image = self.articles[indexPath.row].image
            newArticleViewController.desc = self.articles[indexPath.row].description
            newArticleViewController.titles = self.articles[indexPath.row].title
            newArticleViewController.id = self.articles[indexPath.row].id
            self.navigationController?.pushViewController(newArticleViewController, animated: true)
        }
        editButton.image = UIImage(systemName: "pencil")
        editButton.backgroundColor = .green
        return UISwipeActionsConfiguration(actions: [editButton])
    }
    
    //MARK: - Custom Swipe to delete
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let deleteButton = UIContextualAction(style: .normal, title: "") { (action, _, _) in
            
            SweetAlert().showAlert("Are you sure?", subTitle: "This Article will permanently delete!", style: AlertStyle.warning, buttonTitle:"Cancel", buttonColor:UIColor.colorFromRGB(0xD0D0D0) , otherButtonTitle:  "Delete", otherButtonColor: UIColor.colorFromRGB(0xDD6B55)) { (isOtherButton) -> Void in
                if isOtherButton == true {
                    SweetAlert().showAlert("Cancelled", subTitle: "This Article is safe now", style: AlertStyle.error)
                    
                }
                else {
                    self.articles.remove(at: indexPath.row)
                    self.articleViewModel.deleteArticle(id: self.articles[indexPath.row].id)
                    self.tableView.reloadData()
                    SweetAlert().showAlert("Deleted", subTitle: "This Article has been deleted", style: AlertStyle.success)
                }
            }
        }
        deleteButton.image = UIImage(systemName: "trash")
        deleteButton.backgroundColor = .red
        return UISwipeActionsConfiguration(actions: [deleteButton])
    }

    //MARK: - Infinit Scroll
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == articles.count - 1{
            page += 1
            articleViewModel.fetchArticle(page: page) { (articles) in
                self.articles += articles
                self.tableView.reloadData()
            }
        }
    }


    //MARK: - SET UP Pull To Refresh
    func setUpPullToRefresh() {
        let refresh = UIRefreshControl()
        refresh.addTarget(self, action: #selector(handleRefresh(_:)), for: .valueChanged)
        refresh.tintColor = .white
        self.tableView.addSubview(refresh)
    }
    
    //MARK: - HANDLE Refresh
    @objc func handleRefresh(_ refresh: UIRefreshControl)  {
        articleViewModel.fetchArticle(page: 1, completionHandler: { [weak self](articles) in
            guard let self = self else {return}
            self.articles = articles
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        refresh.endRefreshing()
        })
    }
    
    
}
