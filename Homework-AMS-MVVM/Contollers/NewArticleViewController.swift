//
//  NewArticleViewController.swift
//  Homework-AMS-MVVM
//
//  Created by Sengly Sun on 12/31/19.
//  Copyright © 2019 Sengly Sun. All rights reserved.
//

import UIKit

class NewArticleViewController: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate {

    
    
    @IBOutlet var createAndEditButton: UIButton!
    @IBOutlet var titleTextField: UITextField!
    @IBOutlet var descriptionTextView: UITextView!
    @IBOutlet var articleImageView: UIImageView!
    
    var id: Int?
    var createdDate: String?
    var titles: String?
    var desc: String?
    var image: String?
    var isEdit = false
    
    private var articleViewModel = ArticleViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.makeNavigationBar(color: .systemTeal)
        setUpTapGesture()
        setUpButton()
        if isEdit{
            setUpEditigView(title: titles!, image: image!, description: desc!)
        }
    }
    
    
    //MARK: - SET UP Button
    func setUpButton(){
        createAndEditButton.makeButtonRound()
        createAndEditButton.setImage(UIImage(systemName: isEdit ? "pencil" : "plus"), for: .normal)
    }
    
    //MARK: - SET UP Editing
    func setUpEditigView(title: String,image: String,description: String) {
        let url = image.contains("http://") ? image : "http://"+image
        let imageURL = URL(string: url)
        self.titleTextField?.text = title
        self.descriptionTextView?.text = description
        self.articleImageView.kf.setImage(with: imageURL!)
    }
    
    @IBAction func createButtonTapped(_ sender: Any) {
        let article = Article()
        let imageData = articleImageView?.image == nil ? UIImage(named: "placeholder")?.pngData() : articleImageView?.image?.pngData()
        article.title = titleTextField?.text == "No title" ? "" : titleTextField?.text
        article.description = descriptionTextView?.text == "" ? "" : descriptionTextView?.text
        if isEdit{
            articleViewModel.updateArticle(updateData: imageData!, id: id!, article: article) { (success) in
                if success {
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }else{
            articleViewModel.postArticle(uploadData: imageData!, article: article) { (success) in
                if success {
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
        
        
    }

    //MARK: - Set up tap gesture
    func setUpTapGesture() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapGesture(_:)))
        tap.numberOfTapsRequired = 1
        tap.numberOfTouchesRequired = 1
        articleImageView.addGestureRecognizer(tap)
    }
    
    //MARK: - Function handle to tap gesture
    @objc func tapGesture(_ gesture: UITapGestureRecognizer){
        
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        
        let actionSheet = UIAlertController(title: "Choose Photo", message: nil, preferredStyle: .actionSheet)
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        let photoLibrary = UIAlertAction(title: "Photo Library", style: .default) { (_) in
            imagePicker.sourceType = .photoLibrary
            print("Photo Library")
            self.present(imagePicker, animated: true, completion: nil)
        }
        let camera = UIAlertAction(title: "Camera", style: .default) { (_) in
            imagePicker.sourceType = .camera
            self.present(imagePicker, animated: true, completion: nil)
        }
        
        actionSheet.addAction(camera)
        actionSheet.addAction(cancel)
        actionSheet.addAction(photoLibrary)
        
        self.present(actionSheet, animated: true, completion: nil)

    }
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let image = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        articleImageView?.image = image
        dismiss(animated: true, completion: nil)
    }

    @IBAction func sucessAlert(_ sender: AnyObject) {
        _ = SweetAlert().showAlert(isEdit ? "Update Article":"Add Article", subTitle: isEdit ? "You have been updated an Article" : "You have been added a new Article", style: AlertStyle.success)
    }
}
