//
//  ViewController.swift
//  Homework-AMS-MVVM
//
//  Created by Sengly Sun on 12/30/19.
//  Copyright © 2019 Sengly Sun. All rights reserved.
//

import UIKit
import Kingfisher
class ViewController: UIViewController {

    var page:Int = 1
    var articles = [ArticleModel]()
    var articleViewModel = ArticleViewModle()
    
    var service = ArticleService()
    
    @IBOutlet var imagView: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        articleViewModel.fetchArticle(page: page) { [weak self] (articles) in
            guard let self = self else{return}
            self.articles = articles
            print("Fetch Article: \(articles)")
            
        }
        imagView.kf.setImage(with: URL(string: "http://cdn.sabay.com/cdn/media.sabay.com/media/sabay-news/Sport-News/International-Sports/soccer/Soccer54/Soccer144/Soccer151/5de75ca3da719_1575443580_small.jpg"))
    }


}

