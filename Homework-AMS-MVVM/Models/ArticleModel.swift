//
//  ArticleModel.swift
//  Homework-AMS-MVVM
//
//  Created by Sengly Sun on 12/30/19.
//  Copyright © 2019 Sengly Sun. All rights reserved.
//

import Foundation
class ArticleModel {
    var id: Int
    var title: String
    var description: String
    var createdDate: String
    var image: String
    
    init(article:Article) {
        
        self.id = article.id!
        self.title = article.title!
        self.description = article.description!
        self.image = article.image!
        let dateToformat = DateFormatter()
        dateToformat.dateFormat = "yyyyMMddHHmmss"
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM dd, yyyy"
        let date = dateToformat.date(from: article.createdDate!)
        self.createdDate = dateFormatter.string(from: date!)
        
    }
}
