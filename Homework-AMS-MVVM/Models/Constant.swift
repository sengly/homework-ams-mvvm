//
//  Constant.swift
//  Homework-AMS-MVVM
//
//  Created by Sengly Sun on 12/30/19.
//  Copyright © 2019 Sengly Sun. All rights reserved.
//

import Foundation
enum Config{
    private static let MAIN_URL = "http://110.74.194.124:15011/"
    static let FETCH_URL = MAIN_URL + "v1/api/articles?page="
    static let ARTICLE_URL = MAIN_URL + "v1/api/articles/"
    static let UPLOAD_URL = MAIN_URL + "v1/api/uploadfile/single"
}
