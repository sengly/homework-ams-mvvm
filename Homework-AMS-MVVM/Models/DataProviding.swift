//
//  DataProviding.swift
//  Homework-AMS-MVVM
//
//  Created by Sengly Sun on 12/30/19.
//  Copyright © 2019 Sengly Sun. All rights reserved.
//

import Foundation

//MARK: - Data providing class
struct DataProviding: Codable{
    let code, message: String
    let data: [Article]
    
    enum CodingKeys: String, CodingKey {
        case code = "CODE"
        case message = "MESSAGE"
        case data = "DATA"
    }
}
