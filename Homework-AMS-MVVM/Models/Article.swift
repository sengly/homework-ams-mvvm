//
//  Article.swift
//  Homework-AMS-MVVM
//
//  Created by Sengly Sun on 12/30/19.
//  Copyright © 2019 Sengly Sun. All rights reserved.
//

import Foundation
class Article: Codable {
    var id: Int?
    var title: String?
    var description: String?
    var createdDate: String?
    var image: String?
    
    enum CodingKeys: String, CodingKey {
        case id = "ID"
        case title = "TITLE"
        case description = "DESCRIPTION"
        case createdDate = "CREATED_DATE"
        case image = "IMAGE"
    }
    
    init() {}
    init(id: Int, title: String, description: String, createdDate: String, image: String) {
        self.id = id
        self.title = title
        self.description = description
        self.createdDate = createdDate
        self.image = image
    }
}
